package test.com.cs.clipMe.dal;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.sql.ResultSet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.dal.IUserDAO;
import com.cs.clipMe.dal.UserDM;


public class GetUserTest {

	@Mock
	IUserDAO data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		UserDM user = new UserDM();
		ResultSet myRs = null;
		int id=2;
		when(data.getUser(myRs,id)).thenReturn(user);
		//act
		UserDM user1 = data.getUser(myRs, id);
		//assert
		assertSame(user,user1);
	}


}
