package test.com.cs.clipMe.dal;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.dal.IExtensionDAO;
import com.cs.clipMe.dal.ExtensionDM;

public class ReadExtensionTest {

	@Mock
	IExtensionDAO data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		List<ExtensionDM> extensions = new ArrayList<ExtensionDM>(){{
			add(new ExtensionDM("14","Purple","Wavy",60,30));
		}};
		when(data.readExtension()).thenReturn(extensions);
		//act
		List<ExtensionDM> extensions1 = data.readExtension();
		//assert
		assertSame(extensions,extensions1);
	}

}
