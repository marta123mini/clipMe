package test.com.cs.clipMe.dal;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.dal.IUserDAO;
import com.cs.clipMe.dal.UserDM;

public class UpdateUserTest {
	@Mock
	IUserDAO data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		UserDM user = new UserDM();
		//act
		data.updateUser(user);
		//assert
		verify(data,times(1)).updateUser(user);
	}

}
