package test.com.cs.clipMe.dal;

import org.mockito.Mock;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import com.cs.clipMe.dal.IExtensionDAO;
import com.cs.clipMe.dal.ExtensionDM;

public class CreateExtensionTest {
	@Mock
	IExtensionDAO data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		ExtensionDM extension = new ExtensionDM();
		//act
		data.createExtension(extension);
		//assert
		verify(data,times(1)).createExtension(extension);
	}

}
