package test.com.cs.clipMe.dal;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.jni.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.dal.IUserDAO;
import com.cs.clipMe.dal.UserDM;


public class GetUsersTest {

	@Mock
	IUserDAO data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		ResultSet myRs = null;
		List<UserDM> users = new ArrayList<UserDM>(){{
			add(new UserDM("k@shmir","99945","115 Maplewood Drive","","Jacksonville","FL",32205,"Robert","Plant",1111111111111111L,true,false,false));
		}};
		when(data.getUsers(myRs)).thenReturn(users);
		//act
		List<UserDM> users1 = data.getUsers(myRs);
		//assert
		assertSame(users,users1);
	}


}
