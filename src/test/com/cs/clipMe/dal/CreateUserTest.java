package test.com.cs.clipMe.dal;

import org.mockito.Mock;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import com.cs.clipMe.dal.IUserDAO;
import com.cs.clipMe.dal.UserDM;

public class CreateUserTest {
	@Mock
	IUserDAO data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		UserDM user = new UserDM();
		//act
		data.createUser(user);
		//assert
		verify(data,times(1)).createUser(user);
	}

}
