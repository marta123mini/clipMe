package test.com.cs.clipMe.dal;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.sql.ResultSet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.dal.IExtensionDAO;
import com.cs.clipMe.dal.ExtensionDM;


public class GetExtensionTest {

	@Mock
	IExtensionDAO data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		ExtensionDM extension = new ExtensionDM();
		ResultSet myRs = null;
		int id=2;
		when(data.getExtension(myRs,id)).thenReturn(extension);
		//act
		ExtensionDM extension1 = data.getExtension(myRs, id);
		//assert
		assertSame(extension,extension1);
	}


}
