package test.com.cs.clipMe.dal;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.dal.IExtensionDAO;
import com.cs.clipMe.dal.ExtensionDM;

public class UpdateExtensionTest {
	@Mock
	IExtensionDAO data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		ExtensionDM extension = new ExtensionDM();
		//act
		data.updateExtension(extension);
		//assert
		verify(data,times(1)).updateExtension(extension);
	}

}
