package test.com.cs.clipMe.dal;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.dal.IUserDAO;
import com.cs.clipMe.dal.UserDM;


public class GetUserByIdTest {

	@Mock
	IUserDAO data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		UserDM user = new UserDM();
		int id=2;
		when(data.getUserById(id)).thenReturn(user);
		//act
		UserDM user1 = data.getUserById(id);
		//assert
		assertSame(user,user1);
	}
}
