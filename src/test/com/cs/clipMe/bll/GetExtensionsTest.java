package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.ExtensionSM;
import com.cs.clipMe.bll.IExtensionLogic;


public class GetExtensionsTest {
	@Mock
	IExtensionLogic data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void getExtensionsTest() {
		//arrange
		List<ExtensionSM> fakeList = new ArrayList<ExtensionSM>(){{
			add(new ExtensionSM("14","Purple","Wavy",60,30));
			add(new ExtensionSM("12","Purple","Wavy",60,30));
			add(new ExtensionSM("10","Purple","Wavy",60,30));
		}};
		when(data.getExtensions()).thenReturn(fakeList);
		//act
		List<ExtensionSM> newList = data.getExtensions();
		//assert
		assertSame(fakeList,newList);
	}

}
