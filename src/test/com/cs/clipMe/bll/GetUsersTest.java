package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.UserSM;
import com.cs.clipMe.bll.IUserLogic;

public class GetUsersTest {
	@Mock
	IUserLogic data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		List<UserSM> fakeList = new ArrayList<UserSM>(){{
			add(new UserSM("k@shmir","99945","115 Maplewood Drive","","Jacksonville","FL",32205,"Robert","Plant",1111111111111111L,true,false,false));
		}};
		when(data.getUsers()).thenReturn(fakeList);
		//act
		List<UserSM> newList = data.getUsers();
		//assert
		assertSame(fakeList,newList);
	}
}