package test.com.cs.clipMe.bll;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.ExtensionSM;
import com.cs.clipMe.bll.IExtensionLogic;

public class UpdateExtensionTest {
 @Mock
 IExtensionLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test() {
	 //ARRANGE
	 ExtensionSM fake = new ExtensionSM();
	 //ACT
	 data.updateExtension(fake);
	 //ASSERT
	 verify(data,times(1)).updateExtension(fake);
 }
}
