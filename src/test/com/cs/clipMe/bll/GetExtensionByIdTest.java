package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.ExtensionSM;
import com.cs.clipMe.bll.IExtensionLogic;

public class GetExtensionByIdTest {
	@Mock
	IExtensionLogic data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		ExtensionSM fakeId = new ExtensionSM();
		List<ExtensionSM> fakeList = new ArrayList<ExtensionSM>(){{
			add(new ExtensionSM(2));
			add(new ExtensionSM(3));
			add(new ExtensionSM(4));
		}};
		when(data.getExtensionById(2)).thenReturn(fakeId);
		//act
		ExtensionSM newId = data.getExtensionById(2);
		//assert
		assertSame(fakeId,newId);
	}

}
