package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.UserSM;
import com.cs.clipMe.bll.IUserLogic;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GetUserTest {
 @Mock
 IUserLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test() {
  //Arrange
  UserSM fake = new UserSM();
  
  List<UserSM> otherFake = new ArrayList<UserSM>(){{
	  add(new UserSM("k@shmir","99945","115 Maplewood Drive","","Jacksonville","FL",32205,"Robert","Plant",1111111111111111L,true,false,false));
  }};
  
 when(data.getUser("k@shmir","99945")).thenReturn(fake);
  //Act
 UserSM fakeUser = data.getUser("k@shmir","99945");
 //Assert
 assertSame(fake,fakeUser);
 }
}