package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.UserSM;
import com.cs.clipMe.bll.IUserLogic;
import com.cs.clipMe.dal.UserDM;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ListUserSM_MapTest {
 @Mock
 IUserLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test() {
  //Arrange
  List<UserDM> fake = new ArrayList<UserDM>(){{
	  add(new UserDM("k@shmir","99945","115 Maplewood Drive","","Jacksonville","FL",32205,"Robert","Plant",1111111111111111L,true,false,false));
  }};
  List<UserSM> otherFake = new ArrayList<UserSM>(){{
	  add(new UserSM("k@shmir","99945","115 Maplewood Drive","","Jacksonville","FL",32205,"Robert","Plant",1111111111111111L,true,false,false));
  }};
  
 when(data.map(fake)).thenReturn(otherFake);
  //Act
 List<UserSM> fakeUser = data.map(fake);
 //Assert
 assertSame(otherFake,fakeUser);
 }
}