package test.com.cs.clipMe.bll;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import com.cs.clipMe.bll.IUserLogic;
import com.cs.clipMe.dal.UserDM;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class ReadUserTest {
	@Mock
	IUserLogic data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test(){
		//ARRANGE
		
		List<UserDM> fakeUsers = new ArrayList<UserDM>(){{
			add(new UserDM("k@shmir","99945","115 Maplewood Drive","","Jacksonville","FL",32205,"Robert","Plant",1111111111111111L,true,false,false));
		}};
		//ACT
		data.readUser();
		//ASSERT
		verify(data,times(1)).readUser();
	}
	}
