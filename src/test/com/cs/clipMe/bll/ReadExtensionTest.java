package test.com.cs.clipMe.bll;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import com.cs.clipMe.bll.IExtensionLogic;
import com.cs.clipMe.dal.ExtensionDM;
import com.cs.clipMe.dal.IExtensionDAO;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class ReadExtensionTest {
	@Mock
	IExtensionLogic data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test(){
		//ARRANGE
		
		List<ExtensionDM> fakeExtensions = new ArrayList<ExtensionDM>(){{
			add(new ExtensionDM("14","Purple","Wavy",60,30));
		}};
		//ACT
		data.readExtension();
		//ASSERT
		verify(data,times(1)).readExtension();
	}
	}

