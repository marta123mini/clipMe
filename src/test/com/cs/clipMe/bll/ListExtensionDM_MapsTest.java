package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.ExtensionSM;
import com.cs.clipMe.bll.IExtensionLogic;
import com.cs.clipMe.dal.ExtensionDM;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ListExtensionDM_MapsTest {
 @Mock
 IExtensionLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test() {
  //Arrange
  List<ExtensionDM> fake = new ArrayList<ExtensionDM>(){{
	  add(new ExtensionDM("14","Purple","Wavy",60,30));
  }};
  List<ExtensionSM> otherFake = new ArrayList<ExtensionSM>(){{
	  add(new ExtensionSM("14","Purple","Wavy",60,30));
  }};
  
 when(data.maps(otherFake)).thenReturn(fake);
  //Act
 List<ExtensionDM> fakeExtension = data.maps(otherFake);
 //Assert
 assertSame(fake,fakeExtension);
 }
}