package test.com.cs.clipMe.bll;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.ExtensionSM;
import com.cs.clipMe.bll.IExtensionLogic;

public class DeleteExtensionByIdTest {
	@Mock
	IExtensionLogic data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		List<ExtensionSM> fakeList = new ArrayList<ExtensionSM>(){{
			add(new ExtensionSM(2));
			add(new ExtensionSM(3));
			add(new ExtensionSM(4));
		}};
		//act
		data.deleteExtensionById(2);
		//assert
		verify(data,times(1)).deleteExtensionById(2);
	}

}
