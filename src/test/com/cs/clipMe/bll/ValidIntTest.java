package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.IUserLogic;


public class ValidIntTest {
 @Mock
 IUserLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test(){
  String num = "3";
 when(data.validInt(num)).thenReturn(3);
 
 int newInt = data.validInt(num); 
 
 assertSame(3,newInt);
 }
}