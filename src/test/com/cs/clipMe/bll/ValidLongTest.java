package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.IUserLogic;

import static org.mockito.Mockito.when;

public class ValidLongTest {
	@Mock
	IUserLogic data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//Arrange
		String num = "1234567890";
		when(data.validLong(num)).thenReturn(1234567890L);
		//Act
		long newLong = data.validLong(num);
		//Assert
		assertEquals(1234567890L,newLong);
	}

}

	
	
	
	
	