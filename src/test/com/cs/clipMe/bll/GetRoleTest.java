package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.UserSM;
import com.cs.clipMe.bll.IUserLogic;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GetRoleTest {
 @Mock
 IUserLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test() {
  //Arrange
  UserSM fake = new UserSM();
  
 when(data.getRole(fake)).thenReturn("user");
  //Act
 String fakeUser = data.getRole(fake);
 //Assert
 assertSame("user",fakeUser);
 }
}