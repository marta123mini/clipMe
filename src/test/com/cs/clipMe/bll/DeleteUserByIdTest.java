package test.com.cs.clipMe.bll;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.UserSM;
import com.cs.clipMe.bll.IUserLogic;

public class DeleteUserByIdTest {
	@Mock
	IUserLogic data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		List<UserSM> fakeList = new ArrayList<UserSM>(){{
			add(new UserSM(2));
			add(new UserSM(3));
			add(new UserSM(4));
		}};
		//act
		data.deleteUserById(2);
		//assert
		verify(data,times(1)).deleteUserById(2);
	}

}
