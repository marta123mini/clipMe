package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.UserSM;
import com.cs.clipMe.bll.IUserLogic;
import com.cs.clipMe.dal.UserDM;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class UserSM_MapTest {
 @Mock
 IUserLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test() {
  //Arrange
  UserDM fake = new UserDM();
  UserSM otherFake = new UserSM();
		  
 when(data.map(fake)).thenReturn(otherFake);
  //Act
 UserSM fakeUser = data.map(fake);
 //Assert
 assertSame(otherFake,fakeUser);
 }
}