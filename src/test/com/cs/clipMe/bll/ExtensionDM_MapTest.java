package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.ExtensionSM;
import com.cs.clipMe.bll.IExtensionLogic;
import com.cs.clipMe.dal.ExtensionDM;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ExtensionDM_MapTest {
 @Mock
 IExtensionLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test() {
  //Arrange
  ExtensionDM fake = new ExtensionDM();
  ExtensionSM otherFake = new ExtensionSM();
		  
 when(data.map(otherFake)).thenReturn(fake);
  //Act
 ExtensionDM fakeExtension = data.map(otherFake);
 //Assert
 assertSame(fake,fakeExtension);
 }
}