package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.IUserLogic;

import static org.mockito.Mockito.when;

public class ValidStringTest {
 
 @Mock
 IUserLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test() {
  String input = "valid";
  //Arrange
  when(data.validString(input)).thenReturn("valid");
  //Act
  String newInput = data.validString(input);
  //Assert
  assertSame(input, newInput);
 }
  
}