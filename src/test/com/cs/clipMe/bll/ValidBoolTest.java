package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.IUserLogic;


public class ValidBoolTest {
 @Mock
 IUserLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test(){
  boolean value = true;
 when(data.validBool("true")).thenReturn(value);
 
 boolean newValue = data.validBool("true"); 
 
 assertSame(value,newValue);
 }
}