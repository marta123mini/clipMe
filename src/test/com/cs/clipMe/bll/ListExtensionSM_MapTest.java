package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.ExtensionSM;
import com.cs.clipMe.bll.IExtensionLogic;
import com.cs.clipMe.dal.ExtensionDM;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ListExtensionSM_MapTest {
 @Mock
 IExtensionLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test() {
  //Arrange
  List<ExtensionDM> fake = new ArrayList<ExtensionDM>(){{
	  add(new ExtensionDM("14","Purple","Wavy",60,30));
  }};
  List<ExtensionSM> otherFake = new ArrayList<ExtensionSM>(){{
	  add(new ExtensionSM("14","Purple","Wavy",60,30));
  }};
  
 when(data.map(fake)).thenReturn(otherFake);
  //Act
 List<ExtensionSM> fakeExtension = data.map(fake);
 //Assert
 assertSame(otherFake,fakeExtension);
 }
}