package test.com.cs.clipMe.bll;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.UserSM;
import com.cs.clipMe.bll.IUserLogic;

public class CreateUserTest {
 @Mock
 IUserLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test() {
	 //ARRANGE
	 UserSM fake = new UserSM();
	 //ACT
	 data.createUser(fake);
	 //ASSERT
	 verify(data,times(1)).createUser(fake);
 }
}
