package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.UserSM;
import com.cs.clipMe.bll.IUserLogic;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class SetRolesTest {
 @Mock
 IUserLogic data;
 @Before
 public void setup(){
  initMocks(this);
 }
 @Test
 public void test() {
  //Arrange
  UserSM fake = new UserSM();
  
  int role = 1;
  
 when(data.setRoles(role,fake)).thenReturn(fake);
  //Act
 UserSM fakeUser = data.setRoles(1,fake);
 //Assert
 assertSame(fake,fakeUser);
 }
}