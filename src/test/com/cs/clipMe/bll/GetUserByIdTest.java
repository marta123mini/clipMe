package test.com.cs.clipMe.bll;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.cs.clipMe.bll.UserSM;
import com.cs.clipMe.bll.IUserLogic;

public class GetUserByIdTest {
	@Mock
	IUserLogic data;
	@Before
	public void setup(){
		initMocks(this);
	}
	@Test
	public void test() {
		//arrange
		UserSM fakeId = new UserSM();
		List<UserSM> fakeList = new ArrayList<UserSM>(){{
			add(new UserSM(2));
			add(new UserSM(3));
			add(new UserSM(4));
		}};
		when(data.getUserById(2)).thenReturn(fakeId);
		//act
		UserSM newId = data.getUserById(2);
		//assert
		assertSame(fakeId,newId);
	}

}