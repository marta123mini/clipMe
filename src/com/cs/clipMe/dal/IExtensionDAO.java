package com.cs.clipMe.dal;

import java.sql.ResultSet;
import java.util.List;

public interface IExtensionDAO {

	public abstract void createExtension(ExtensionDM extension);
	
	public abstract ExtensionDM getExtension(ResultSet myRs, int id);
	
	public abstract List<ExtensionDM> getExtensions(ResultSet myRs);

	public abstract ExtensionDM getExtensionById(int id);

	public abstract List<ExtensionDM> readExtension();

	public abstract void updateExtension(ExtensionDM extension);

	public abstract void deleteExtensionById(int id);

}