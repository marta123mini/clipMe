package com.cs.clipMe.dal;

import java.sql.ResultSet;
import java.util.List;

public interface IUserDAO {

	public abstract void createUser(UserDM user);

	public abstract List<UserDM> getUsers(ResultSet myRs);

	public abstract UserDM getUserById(int id);

	public abstract List<UserDM> readUser();

	public abstract void updateUser(UserDM user);

	public abstract void deleteUserById(int id);
	
	public abstract UserDM getUser(ResultSet myRs, int id);

}