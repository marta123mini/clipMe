package com.cs.clipMe.dal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.cs.clipMe.applicationlogger.ILoggerIO;

public class ExtensionDAO implements IExtensionDAO{
	private ILoggerIO logs;

	public ExtensionDAO(ILoggerIO log){
		this.logs = log;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IExtensionDAO#createExtension(com.cs.clipMe.dal.ExtensionDM)
	 */
	@Override
	public void createExtension(ExtensionDM extension){
		Connection con = null;
		CallableStatement cs = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/clipme","root","Onshore09!");
			cs = con.prepareCall("{call createExtension(?,?,?,?,?)}");
			cs.setString(1, extension.length);
			cs.setString(2, extension.color);
			cs.setString(3, extension.texture);
			cs.setInt(4, extension.price);
			cs.setInt(5, extension.quantity);
			cs.execute();
			logs.logError("Event","I was able to create a extension","Class:ExtensionDAO & Method Name:createExtension");
		}catch(Exception e){
			logs.logError("Error","I was unable to create a extension","Class:ExtensionDAO & Method Name:createExtension");
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IExtensionDAO#getExtensions(com.cs.clipMe.dal.ExtensionDM)
	 */
	@Override
	public List<ExtensionDM> getExtensions(ResultSet myRs){
		List<ExtensionDM> extensions = new ArrayList<ExtensionDM>();
		try{
			while (myRs.next()){
				ExtensionDM ext = new ExtensionDM();
				ext.setLength(myRs.getString("length"));
				ext.setColor(myRs.getString("color"));
				ext.setTexture(myRs.getString("texture"));
				ext.setPrice(myRs.getInt("price"));
				ext.setQuantity(myRs.getInt("quantity"));
				ext.setId(myRs.getInt("id"));
				extensions.add(ext);
			}
			logs.logError("Event","I was able to get a list of extensions","Class:ExtensionDAO & Method Name:getExtensions");
			return extensions;
		}catch(SQLException e){
			logs.logError("Error","I was unable to get a list of extensions","Class:ExtensionDAO & Method Name:getExtensions");
		}
		return null;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IExtensionDAO#getExtension(com.cs.clipMe.dal.ExtensionDM)
	 */
	@Override
	public ExtensionDM getExtension(ResultSet myRs, int id){
		ExtensionDM ext = new ExtensionDM();
		try{
			while (myRs.next()){
				ext.setId(myRs.getInt("id"));
				if (ext.id == id){
					ext.setLength(myRs.getString("length"));
					ext.setColor(myRs.getString("color"));
					ext.setTexture(myRs.getString("texture"));
					ext.setPrice(myRs.getInt("price"));
					ext.setQuantity(myRs.getInt("quantity"));
					return ext;
				}
			}
			logs.logError("Event","I was able to get an extension","Class:ExtensionDAO & Method Name:getExtension");
		}catch(SQLException e){
			logs.logError("Error","I was unable to get an extension","Class:ExtensionDAO & Method Name:getExtension");
		}
		return null;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IExtensionDAO#getExtensionById(int)
	 */
	@Override
	public ExtensionDM getExtensionById(int id){
		ExtensionDM extension = new ExtensionDM();
		Connection con = null;
		CallableStatement cs = null;
		ResultSet myRs = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/clipme","root","Onshore09!");
			cs = con.prepareCall("{call getExtensionById(?)}");
			cs.setInt(1, id);
			cs.execute();
			logs.logError("Event","I was able to get an extension by id","Class:ExtensionDAO & Method Name:getExtensionById");
			myRs = cs.getResultSet();
			extension = getExtension(myRs, id);
			return extension;
		}catch(Exception e){
			logs.logError("Error","I was unable to get an extension by id","Class:ExtensionDAO & Method Name:getExtensionById");
			return null;
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IExtensionDAO#readExtension()
	 */
	@Override
	public List<ExtensionDM> readExtension(){
		List<ExtensionDM> extensions = new ArrayList<ExtensionDM>();
		Connection con = null;
		CallableStatement cs = null;
		ResultSet myRs = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/clipme","root","Onshore09!");
			cs = con.prepareCall("{call getExtensions()}");
			cs.execute();
			myRs = cs.getResultSet();
			extensions = getExtensions(myRs);
			logs.logError("Event","I was able to read an extension","Class:ExtensionDAO & Method Name:readExtension");
			return extensions;
		}catch(Exception e){
			logs.logError("Error","I was unable to read an extension","Class:ExtensionDAO & Method Name:readExtension");
			return null;
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IExtensionDAO#updateExtension(com.cs.clipMe.dal.ExtensionDM)
	 */
	@Override
	public void updateExtension(ExtensionDM extension){
		Connection con = null;
		CallableStatement cs = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/clipme","root","Onshore09!");
			cs = con.prepareCall("{call updateExtension(?,?,?,?,?,?)}");
			cs.setString(1, extension.length);
			cs.setString(2, extension.color);
			cs.setString(3, extension.texture);
			cs.setInt(4, extension.price);
			cs.setInt(5, extension.quantity);
			cs.setInt(6, extension.id);
			cs.execute();
			logs.logError("Event","I was able to update an extension","Class:ExtensionDAO & Method Name:updateExtension");
		}catch(Exception e){
			logs.logError("Error","I was unable to update an extension","Class:ExtensionDAO & Method Name:updateExtension");
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IExtensionDAO#deleteExtensionById(int)
	 */
	@Override
	public void deleteExtensionById(int id){
		Connection con = null;
		CallableStatement cs = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/clipme","root","Onshore09!");
			cs = con.prepareCall("{call deleteExtensionById(?)}");
			cs.setInt(1, id);
			cs.execute();
			logs.logError("Event","I was able to delete an extension by id","Class:ExtensionDAO & Method Name:deleteExtensionById");
		}catch(Exception e){
			logs.logError("Error","I was unable to delete an extension by id","Class:ExtensionDAO & Method Name:deleteExtensionById");
		}
	}
}