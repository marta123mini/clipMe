package com.cs.clipMe.dal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.cs.clipMe.applicationlogger.*;

public class UserDAO implements IUserDAO{
	private ILoggerIO logs;
	
	public UserDAO(ILoggerIO log){
		this.logs=log;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IUserDAO#createUser(com.cs.clipMe.dal.UserDM)
	 */
	@Override
	public void createUser(UserDM user){
		Connection con = null;
		CallableStatement cs = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/clipme","root","Onshore09!");
			cs=con.prepareCall("{call createUser(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			cs.setString(1, user.username);
			cs.setString(2, user.password);
			cs.setString(3, user.street1);
			cs.setString(4, user.street2);
			cs.setString(5, user.city);
			cs.setString(6, user.state);
			cs.setInt(7, user.zipcode);
			cs.setString(8, user.firstname);
			cs.setString(9, user.lastname);
			cs.setLong(10, user.ccn);
			cs.setBoolean(11, user.customer);
			cs.setBoolean(12, user.employee);
			cs.setBoolean(13, user.admin);
			cs.execute();
			logs.logError("Event","I was able to create a user","Class:UserDAO & Method Name:createUser");
		}catch(Exception e){
			logs.logError("Error","I was unable to create a user","Class:UserDAO & Method Name:createUser");
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IUserDAO#getUser(com.cs.clipMe.dal.UserDM)
	 */
	@Override
	public UserDM getUser(ResultSet myRs, int id) {
		UserDM usr = new UserDM();
		try{
			while (myRs.next()){
				usr.setId(myRs.getInt("id"));
				if (usr.id == id){
					usr.setUsername(myRs.getString("username"));
					usr.setPassword(myRs.getString("password"));
					usr.setStreet1(myRs.getString("street1"));
					usr.setStreet2(myRs.getString("street2"));
					usr.setCity(myRs.getString("city"));
					usr.setState(myRs.getString("state"));
					usr.setZipcode(myRs.getInt("zipcode"));
					usr.setFirstname(myRs.getString("firstname"));
					usr.setLastname(myRs.getString("lastname"));
					usr.setCcn(myRs.getLong("ccn"));
					usr.setCustomer(myRs.getBoolean("customer"));
					usr.setEmployee(myRs.getBoolean("employee"));
					usr.setAdmin(myRs.getBoolean("admin"));
					return usr;
				}
			}
			logs.logError("Event","I was able to get a user","Class:UserDAO & Method Name:getUser");
		}catch(SQLException e){
			logs.logError("Error","I was unable to get a user","Class:UserDAO & Method Name:getUser");
		}
		return null;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IUserDAO#getUsers(java.sql.ResultSet)
	 */
	@Override
	public List<UserDM> getUsers(ResultSet myRs){
		List<UserDM> users = new ArrayList<UserDM>();
		try{
			while(myRs.next()){
				UserDM usr=new UserDM();
				usr.setUsername(myRs.getString("username"));
				usr.setPassword(myRs.getString("password"));
				usr.setStreet1(myRs.getString("street1"));
				usr.setStreet2(myRs.getString("street2"));
				usr.setCity(myRs.getString("city"));
				usr.setState(myRs.getString("state"));
				usr.setZipcode(myRs.getInt("zipcode"));
				usr.setFirstname(myRs.getString("firstname"));
				usr.setLastname(myRs.getString("lastname"));
				usr.setCcn(myRs.getLong("ccn"));
				usr.setCustomer(myRs.getBoolean("customer"));
				usr.setEmployee(myRs.getBoolean("employee"));
				usr.setAdmin(myRs.getBoolean("admin"));
				usr.setId(myRs.getInt("id"));
				users.add(usr);
			}
			logs.logError("Event","I was able to get a list of users","Class:UserDAO & Method Name:getUsers");
			return users;
		}catch(SQLException e){
			logs.logError("Error","I was unable to get a list of users","Class:UserDAO & Method Name:getUsers");
		}
		return null;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IUserDAO#getUserById(int)
	 */
	@Override
	public UserDM getUserById(int id){
		UserDM user = new UserDM();
		Connection con=null;
		CallableStatement cs=null;
		ResultSet myRs=null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/clipme","root","Onshore09!");
			cs=con.prepareCall("{call getUserById(?)}");
			cs.setInt(1, id);
			cs.execute();
			logs.logError("Event","I was able to get a user by Id","Class:UserDAO & Method Name:getUserById");
			myRs=cs.getResultSet();
			user = getUser(myRs, id);
			return user;
		}catch(Exception e){
			logs.logError("Error","I was unable to get a user by Id","Class:UserDAO & Method Name:getUserById");
			return null;
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IUserDAO#readUser()
	 */
	@Override
	public List<UserDM> readUser() {
		List<UserDM> users = new ArrayList<UserDM>();
		Connection con = null;
		CallableStatement cs=null;
		ResultSet myRs=null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/clipme","root","Onshore09!");
			cs=con.prepareCall("{call getUsers()}");
			cs.execute();
			logs.logError("Event","I was able to read a user","Class:UserDAO & Method Name:readUser");
			myRs=cs.getResultSet();
			users=getUsers(myRs);
			return users;
		}catch(Exception e){
			logs.logError("Error","I was unable to read a user","Class:UserDAO & Method Name:readUser");
			return null;
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IUserDAO#updateUser(com.cs.clipMe.dal.UserDM)
	 */
	@Override
	public void updateUser(UserDM user){
		Connection con = null;
		CallableStatement cs =null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/clipme","root","Onshore09!");
			cs =con.prepareCall("{call updateUser(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			cs.setString(1, user.username);
			cs.setString(2, user.password);
			cs.setString(3, user.street1);
			cs.setString(4, user.street2);
			cs.setString(5, user.city);
			cs.setString(6, user.state);
			cs.setInt(7, user.zipcode);
			cs.setString(8, user.firstname);
			cs.setString(9, user.lastname);
			cs.setLong(10, user.ccn);
			cs.setBoolean(11, user.customer);
			cs.setBoolean(12, user.employee);
			cs.setBoolean(13, user.admin);
			cs.setInt(14,user.id);
			cs.execute();
			logs.logError("Event","I was able to update a user","Class:UserDAO & Method Name:updateUser");
		}catch(Exception e){
			logs.logError("Error","I was unable to update a user","Class:UserDAO & Method Name:updateUser");
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.dal.IUserDAO#deleteUserById(int)
	 */
	@Override
	public void deleteUserById(int id) {
		  Connection con = null;
		  CallableStatement cs=null;
		  try{
			  Class.forName("com.mysql.jdbc.Driver");
			  con=DriverManager.getConnection("jdbc:mysql://localhost:3306/clipme","root","Onshore09!");
			  cs=con.prepareCall("{call deleteUserById(?)}");
			  cs.setInt(1, id);
			  cs.execute();
			  logs.logError("Event","I was able to delete a user by id","Class:UserDAO & Method Name:deleteUserById");
		  }catch(Exception e){
			  logs.logError("Error","I was unable to delete a user by id","Class:UserDAO & Method Name:deleteUserById");
		  }
	}
}