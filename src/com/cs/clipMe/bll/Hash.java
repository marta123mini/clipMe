package com.cs.clipMe.bll;

import java.math.BigInteger;
import java.security.MessageDigest;

public class Hash implements IHash{
	
	IHash data;
	public Hash(IHash password){
		this.data=password;
	}
	
	/* (non-Javadoc)
	 * @see test.com.bc.movielibrary.bll.IHash#getHash(java.lang.String)
	 */
	@Override
	public String getHash(String input){
		String md5 = null;
		try{
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(input.getBytes(), 0, input.length());
			md5=new BigInteger(1,digest.digest()).toString(16);
		}catch(Exception e){
			e.printStackTrace();
		}
		return md5;
	}
}
