package com.cs.clipMe.bll;

import java.util.ArrayList;
import java.util.List;

import com.cs.clipMe.applicationlogger.ILoggerIO;
import com.cs.clipMe.dal.ExtensionDM;
import com.cs.clipMe.dal.IExtensionDAO;

public class ExtensionLogic implements IExtensionLogic{
	
	public List<ExtensionSM> extensions = new ArrayList<ExtensionSM>();
	IExtensionDAO data;
	ILoggerIO logs;
	
	public ExtensionLogic(IExtensionDAO extension, ILoggerIO stuff){
		this.data=extension;
		this.logs=stuff;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IExtensionLogic#readExtension()
	 */
	@Override
	public void readExtension(){
		extensions.clear();
		List<ExtensionDM> ext = data.readExtension();
		for(ExtensionDM extension : ext){
			extensions.add(map(extension));
		}
		logs.logError("Event", "I was able to read an extension", "Class:ExtensionLogic & Method Name:readExtension");
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IExtensionLogic#getExtensions()
	 */
	@Override
	public List<ExtensionSM> getExtensions(){
		try{
		readExtension();
		logs.logError("Event", "I was able to get a list of extensions", "Class:ExtensionLogic & Method Name:getExtensions");
		return extensions;
		}catch(Exception e){
			logs.logError("Error", "I was unable to get a list of extensions", "Class:ExtensionLogic & Method Name:getExtensions");
			return null;
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IExtensionLogic#getExtensionById(int)
	 */
	@Override
	public ExtensionSM getExtensionById(int idInput) {
		try{
		 ExtensionSM extension= map(data.getExtensionById(idInput));
				logs.logError("Event", "I was able to get an extension by id", "Class:ExtensionLogic & Method Name:getExtensionById");
				return extension; 
		}catch(Exception e){
			logs.logError("Error", "I was unable to get an extension by id", "Class:ExtensionLogic & Method Name:getExtensionById");
			 return null;	
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IExtensionLogic#createExtension(com.cs.clipMe.bll.ExtensionSM)
	 */
	@Override
	public void createExtension(ExtensionSM extensionView) {
		  try {
		   data.createExtension(map(extensionView));
			logs.logError("Event", "I was able to create an extension", "Class:ExtensionLogic & Method Name:createExtension");
		  } catch (Exception e) {
				logs.logError("Error", "I was unable to create an extension", "Class:ExtensionLogic & Method Name:createExtension");
		  }
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IExtensionLogic#updateExtension(com.cs.clipMe.bll.ExtensionSM)
	 */
	@Override
	public void updateExtension(ExtensionSM extension){
		data.updateExtension(map(extension));
		logs.logError("Event", "I was able to update an extension", "Class:ExtensionLogic & Method Name:updateExtension");
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IExtensionLogic#deleteExtensionById(int)
	 */
	@Override
	public void deleteExtensionById(int idInput) {
		  try{
			  data.deleteExtensionById(idInput);
		  logs.logError("Event", "I was able to delete an extension by id","Class:ExtensionLogic & Method Name: deleteExtensionById");
		  }catch(Exception e){
		  }
		  logs.logError("Error", "I was unable to delete an extension by id", "Class:ExtensionLogic & Method Name: deleteExtensionById");
		 }
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IExtensionLogic#map(com.cs.clipMe.dal.ExtensionDM)
	 */
	@Override
	public ExtensionSM map(ExtensionDM extensions){
		  ExtensionSM newExtension=new ExtensionSM();
		  newExtension.setId(extensions.id);
		  newExtension.setLength(extensions.length);
		  newExtension.setColor(extensions.color);
		  newExtension.setTexture(extensions.texture);
		  newExtension.setPrice(extensions.price);
		  newExtension.setQuantity(extensions.quantity);
		  logs.logError("Event", "I was able to map an extension (ExtensionDM)", "Class:ExtensionLogic & Method Name:map");
		  return newExtension;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IExtensionLogic#map(com.cs.clipMe.bll.ExtensionSM)
	 */
	@Override
	public ExtensionDM map(ExtensionSM extensions){
		  ExtensionDM newExtension=new ExtensionDM();
		  newExtension.setId(extensions.getId());
		  newExtension.setLength(extensions.getLength());
		  newExtension.setColor(extensions.getColor());
		  newExtension.setTexture(extensions.getTexture());
		  newExtension.setPrice(extensions.getPrice());
		  newExtension.setQuantity(extensions.getQuantity());
		  logs.logError("Event", "I was able to map an extension (ExtensionSM)", "Class:ExtensionLogic & Method Name:map");
		  return newExtension;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IExtensionLogic#map(java.util.List)
	 */
	@Override
	public List<ExtensionSM> map(List<ExtensionDM> extensions){
		  List<ExtensionSM> newExtension=new ArrayList<ExtensionSM>();
		  for(ExtensionDM extension:extensions){
			 newExtension.add(map(extension));
		  }
		  logs.logError("Event", "I was able to map a list of extensions (List<ExtensionDM>)", "Class:ExtensionLogic & Method Name:map");
		  return newExtension;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IExtensionLogic#maps(java.util.List)
	 */
	@Override
	public List<ExtensionDM> maps(List<ExtensionSM> extensions){
		  List<ExtensionDM> newExtension = new ArrayList<ExtensionDM>();
		  for (ExtensionSM us : extensions){
		   newExtension.add(map(us));
		  }
		  logs.logError("Event", "I was able to map a list of extensions (List<ExtensionSM>)", "Class:ExtensionLogic & Method Name:maps");
		  return newExtension;
	}
}