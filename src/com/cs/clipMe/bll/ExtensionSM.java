package com.cs.clipMe.bll;

public class ExtensionSM {
	
	public String length;
	public String color;
	public String texture;
	public int price;
	public int quantity;
	public int id;
	
	public ExtensionSM(){
		
	}
	
	public ExtensionSM(String length,String color,String texture,int price,int quantity,int id){
		this.length=length;
		this.color=color;
		this.texture=texture;
		this.price=price;
		this.quantity=quantity;
		this.id=id;
	}
	
	public ExtensionSM(String length,String color,String texture,int price,int quantity){
		this.length=length;
		this.color=color;
		this.texture=texture;
		this.price=price;
		this.quantity=quantity;
	}
	
	public ExtensionSM(int id){
		this.id=id;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTexture() {
		return texture;
	}

	public void setTexture(String texture) {
		this.texture = texture;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}