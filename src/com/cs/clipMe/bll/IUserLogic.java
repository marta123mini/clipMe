package com.cs.clipMe.bll;

import java.util.List;

import com.cs.clipMe.dal.UserDM;

public interface IUserLogic {

	public abstract void readUser();

	public abstract List<UserSM> getUsers();

	public abstract UserSM getUserById(int idInput);

	public abstract void createUser(UserSM userView);

	public abstract void updateUser(UserSM user);

	public abstract UserSM map(UserDM newUser);

	public abstract UserDM map(UserSM newUser);

	public abstract List<UserSM> map(List<UserDM> users);

	public abstract List<UserDM> maps(List<UserSM> users);

	public abstract void deleteUserById(int id);

	public abstract boolean validUserLogin(String usernameInput, String passwordInput);

	public abstract int validInt(String input);

	public abstract long validLong(String input);

	public abstract boolean tryParse(String input);

	public abstract String validString(String input);

	public abstract Boolean validBool(String input);

	public abstract String prompt(String input);

	public abstract UserSM setRoles(int role, UserSM eUser);

	public abstract UserSM getUser(String username, String password);

	public abstract String getRole(UserSM person);

}