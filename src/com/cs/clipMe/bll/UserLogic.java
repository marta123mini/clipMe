package com.cs.clipMe.bll;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.cs.clipMe.dal.*;
import com.cs.clipMe.applicationlogger.ILoggerIO;
import com.cs.clipMe.dal.IUserDAO;
import com.cs.clipMe.bll.IHash;

public class UserLogic implements IUserLogic{
	public List<UserSM> users = new ArrayList<UserSM>();
	IUserDAO data;
	ILoggerIO logs;
	IHash hash;

	public UserLogic(IUserDAO user, ILoggerIO stuff) {
		this.data = user;
		this.logs = stuff;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#readUser()
	 */
	@Override
	public void readUser() {
		users.clear();
		List<UserDM> usr = data.readUser();
		for (UserDM user : usr) {
			users.add(map(user));
		}
		logs.logError("Event", "I was able to read a customer",
				"Class:UserLogic & Method Name:readUser");
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#getUsers()
	 */
	@Override
	public List<UserSM> getUsers() {
		readUser();
		logs.logError("Event", "I was able to get a list of users",
				"Class:UserLogic & Method Name:getUsers");
		return users;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#getUserById(int)
	 */
	@Override
	public UserSM getUserById(int idInput) {
		try {
			UserSM user = map(data.getUserById(idInput));
			logs.logError("Event", "I was able to get a user by id",
					"Class:UserLogic & Method Name:getUserById");
			return user;
		} catch (Exception e) {
			logs.logError("Error", "I was unable to get a user by id",
					"Class:UserLogic & Method Name:getUserById");
			return null;
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#createUser(com.cs.clipMe.bll.UserSM)
	 */
	@Override
	public void createUser(UserSM userView) {
		data.createUser(map(userView));
		logs.logError("Event", "I was able to create a user",
				"Class:UserLogic & Method Name:createUser");
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#updateUser(com.cs.clipMe.bll.UserSM)
	 */
	@Override
	public void updateUser(UserSM user) {
		data.updateUser(map(user));
		logs.logError("Event", "I was able to update a user",
				"Class:UserLogic & Method Name:updateUser");
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#map(com.cs.clipMe.dal.UserDM)
	 */
	@Override
	public UserSM map(UserDM newUser) {
		UserSM user = new UserSM();
		user.setId(newUser.id);
		user.setUsername(newUser.username);
		user.setPassword(newUser.password);
		user.setStreet1(newUser.street1);
		user.setStreet2(newUser.street2);
		user.setCity(newUser.city);
		user.setState(newUser.state);
		user.setZipcode(newUser.zipcode);
		user.setFirstname(newUser.firstname);
		user.setLastname(newUser.lastname);
		user.setCcn(newUser.ccn);
		user.setCustomer(newUser.customer);
		user.setEmployee(newUser.employee);
		user.setAdmin(newUser.admin);
		logs.logError("Event", "I was able to map a user (UserDM)",
				"Class:UserLogic & Method Name: map");
		return user;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#map(com.cs.clipMe.bll.UserSM)
	 */
	@Override
	public UserDM map(UserSM newUser) {
		UserDM user = new UserDM();
		user.setId(newUser.getId());
		user.setUsername(newUser.getUsername());
		user.setPassword(newUser.getPassword());
		user.setStreet1(newUser.getStreet1());
		user.setStreet2(newUser.getStreet2());
		user.setCity(newUser.getCity());
		user.setState(newUser.getState());
		user.setZipcode(newUser.getZipcode());
		user.setFirstname(newUser.getFirstname());
		user.setLastname(newUser.getLastname());
		user.setCcn(newUser.getCcn());
		user.setCustomer(newUser.isCustomer());
		user.setEmployee(newUser.isEmployee());
		user.setAdmin(newUser.isAdmin());
		logs.logError("Event", "I was able to map a user (UserSM)",
				"Class:UserLogic & Method Name: map");
		return user;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#map(java.util.List)
	 */
	@Override
	public List<UserSM> map(List<UserDM> users) {
		List<UserSM> newUser = new ArrayList<UserSM>();
		for (UserDM us : users) {
			newUser.add(map(us));
		}
		logs.logError("Event", "I was able to map a list of users (List<UserDM>)",
				"Class:UserLogic & Method Name: map");
		return newUser;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#maps(java.util.List)
	 */
	@Override
	public List<UserDM> maps(List<UserSM> users) {
		List<UserDM> newUser = new ArrayList<UserDM>();
		for (UserSM us : users) {
			newUser.add(map(us));
		}
		logs.logError("Event", "I was able to map a list of users (List<UserSM)",
				"Class:UserLogic & Method Name: maps");
		return newUser;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#deleteUserById(int)
	 */
	@Override
	public void deleteUserById(int id) {
		try {
			data.deleteUserById(id);
		}catch(Exception e){
			logs.logError("Event", "I was able to delete a user by id",
					"Class:UserLogic & Method Name: deleteUserById");
		}
		logs.logError("Error", "I was unable to delete a user by id",
				"Class:UserLogic & Method Name: deleteUserById");
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#validUserLogin(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean validUserLogin(String usernameInput, String passwordInput) {
		readUser();
		for (UserSM tempUser : users) {
			if (usernameInput.equals(tempUser.username)
					&& passwordInput.equals(tempUser.password)) {
				logs.logError("Event", "I was able to validate a user login",
						"Class:UserLogic & Method Name:validUserLogin");
				return true;
			}
		}
		logs.logError("Error", "I was unable to validate a user login",
				"Class:UserLogic & Method Name:validUserLogin");
		return false;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#validInt(java.lang.String)
	 */
	@Override
	public int validInt(String input) {
		if (tryParse(input)) {
			return Integer.parseInt(input);
		} else {
			System.out.println("invalid input");
			return validInt(prompt("Reenter valid input"));
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#validLong(java.lang.String)
	 */
	@Override
	public long validLong(String input) {
		if (tryParse(input)) {
			return Long.parseLong(input);
		} else {
			System.out.println("invalid input");
			return validInt(prompt("Reenter valid input"));
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#tryParse(java.lang.String)
	 */
	@Override
	public boolean tryParse(String input) {
		try {
			Integer.parseInt(input);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#validString(java.lang.String)
	 */
	@Override
	public String validString(String input) {
		if (!tryParse(input)) {
			return input;
		} else {
			System.out.println("invalid input");
			return validString(prompt("Reenter valid input"));
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#validBool(java.lang.String)
	 */
	@Override
	public Boolean validBool(String input) {
		if (!tryParse(input)) {
			return true;
		} else {
			System.out.println("invalid input");
			return validBool(prompt("Reenter valid input"));
		}
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#prompt(java.lang.String)
	 */
	@Override
	public String prompt(String input) {
		Scanner userInput = new Scanner(System.in);
		System.out.println(input);
		return userInput.nextLine();
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#setRoles(int, com.cs.clipMe.bll.UserSM)
	 */
	@Override
	public UserSM setRoles(int role, UserSM eUser) {
		if (role == 1) {
			eUser.setCustomer(true);
		} else if (role == 2) {
			eUser.setEmployee(true);
		} else if (role == 3) {
			eUser.setAdmin(true);
		}
		logs.logError("Event", "I was able to set user role","Class:UserLogic & Method Name:setRoles");
		return eUser;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#getUser(java.lang.String, java.lang.String)
	 */
	@Override
	public UserSM getUser(String username, String password) {
		for (UserSM user : users) {
			if (user.username.equals(username)
					&& user.password.equals(password)) {
				logs.logError("Event", "I was able to get a user by username and password",
						"Class:UserLogic & Method Name:getUser");
				return user;
			}
		}
		logs.logError("Error", "I was unable to get a user by by username and password",
				"Class:UserLogic & Method Name:getUser");
		return null;
	}
	/* (non-Javadoc)
	 * @see com.cs.clipMe.bll.IUserLogic#getRole(com.cs.clipMe.bll.UserSM)
	 */
	@Override
	public String getRole(UserSM person) {
		if(person.isCustomer()){
			logs.logError("Event", "I was able to get a customer role","Class:UserLogic & Method Name:getRole");
			return "Customer";
		}
		if(person.isEmployee()){
			logs.logError("Event", "I was able to get an employee role","Class:UserLogic & Method Name:getRole");
			return "Employee";
		}else{
			logs.logError("Event", "I was able to get an admin role","Class:UserLogic & Method Name:getRole");
			return "Admin";
		}
	}
}