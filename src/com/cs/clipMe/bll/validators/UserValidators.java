package com.cs.clipMe.bll.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.cs.clipMe.bll.IUserLogic;
import com.cs.clipMe.spring.models.UserFM;

@Component
public class UserValidators implements Validator {
	@Autowired
	@Qualifier("userLog")
	private IUserLogic userLogic;
	
	public void setUserLogic(IUserLogic userLog){
		this.userLogic=userLog;
	}
	@Override
	public boolean supports(Class<?> clazz) {
		return UserFM.class.equals(clazz);
	}
	@Override
	public void validate(Object target, Errors errors) {
		UserFM person=(UserFM) target;
		if(person.getUsername()==null || person.getUsername().length()<4){
			errors.rejectValue("username", "error.empty.field", "Please Enter Username of at least 4 characters");
		}
		if(person.getPassword()==null || person.getPassword().length()<4){
			errors.rejectValue("password", "error.empty.field", "Please Enter a Password of at least 4 characters");
		}else if(!userLogic.validUserLogin(person.getUsername(), person.getPassword())){
			errors.rejectValue("user", "error.unknown.user", "Invalid User");
		}
	}
}