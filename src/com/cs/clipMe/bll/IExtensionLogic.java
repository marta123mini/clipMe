package com.cs.clipMe.bll;

import java.util.List;

import com.cs.clipMe.dal.ExtensionDM;

public interface IExtensionLogic {

	public abstract void readExtension();

	public abstract List<ExtensionSM> getExtensions();

	public abstract ExtensionSM getExtensionById(int idInput);

	public abstract void createExtension(ExtensionSM extensionView);

	public abstract void updateExtension(ExtensionSM extension);

	public abstract void deleteExtensionById(int idInput);

	public abstract ExtensionSM map(ExtensionDM extensions);

	public abstract ExtensionDM map(ExtensionSM extensions);

	public abstract List<ExtensionSM> map(List<ExtensionDM> extensions);

	public abstract List<ExtensionDM> maps(List<ExtensionSM> extensions);

}