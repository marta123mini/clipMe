package com.cs.clipMe.bll;

public class UserSM {
	
	public String username;
	public String password;
	public String street1;
	public String street2;
	public String city;
	public String state;
	public int zipcode;
	public String firstname;
	public String lastname;
	public long ccn;
	public boolean customer;
	public boolean employee;
	public boolean admin;
	public int id;
	
	public UserSM(){
		
	}
	
	public UserSM(String username, String password){
		this.username=username;
		this.password=password;
	}

	public UserSM(String username,String password,String street1,String street2,String city,String state,int zipcode,String firstname,String lastname,long ccn,boolean customer,boolean employee,boolean admin){
		this.username=username;
		this.password=password;
		this.street1=street1;
		this.street2=street2;
		this.city=city;
		this.state=state;
		this.zipcode=zipcode;
		this.firstname=firstname;
		this.lastname=lastname;
		this.ccn=ccn;
		this.customer=customer;
		this.employee=employee;
		this.admin=admin;
	}

	public UserSM(int id,String username,String password,String street1,String street2,String city,String state,int zipcode,String firstname,String lastname,long ccn,boolean customer,boolean employee,boolean admin){
		this.id=id;
		this.username=username;
		this.password=password;
		this.street1=street1;
		this.street2=street2;
		this.city=city;
		this.state=state;
		this.zipcode=zipcode;
		this.firstname=firstname;
		this.lastname=lastname;
		this.ccn=ccn;
		this.customer=customer;
		this.employee=employee;
		this.admin=admin;
	}
	
	public UserSM(int id){
		this.id=id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getZipcode() {
		return zipcode;
	}

	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public long getCcn() {
		return ccn;
	}

	public void setCcn(long ccn) {
		this.ccn = ccn;
	}

	public boolean isCustomer() {
		return customer;
	}

	public void setCustomer(boolean customer) {
		this.customer = customer;
	}

	public boolean isEmployee() {
		return employee;
	}

	public void setEmployee(boolean employee) {
		this.employee = employee;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
