package com.cs.clipMe.applicationlogger;

import java.util.List;

public interface ILoggerIO {

	public abstract void logError(String errorType, String message,
			String location);

	public abstract List<MessageDO> readLogs();

}