package com.cs.clipMe.applicationlogger;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LoggerIO implements ILoggerIO {
	/* (non-Javadoc)
	 * @see applicationLogger.ILoggerIO#logError(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void logError(String errorType, String message, String location){
		try{
			String[] errorMessage = new String[]{new Date().toString()+ " ",errorType+ " ",message+ " ",location+ "\n"};
			for (String msg : errorMessage) {
				Files.write(Paths.get("src","AppLogger"), msg.getBytes(), StandardOpenOption.APPEND);
			}
		}catch(Exception e){
			
		}
	}
	/* (non-Javadoc)
	 * @see applicationLogger.ILoggerIO#readLogs()
	 */
	@Override
	public List<MessageDO> readLogs(){
		try{
			List<String> log = Files.readAllLines(Paths.get("src", "AppLogger"));
			List<MessageDO> logs = new ArrayList<MessageDO>();
			for (String singleMsg : log) {
				MessageDO mes = new MessageDO();
				mes.setDayTime(singleMsg.split(" ")[0]);
				mes.setErrorType(singleMsg.split(" ")[1]);
				mes.setErrorMessage(singleMsg.split(" ")[2]);
				mes.setLayer(singleMsg.split(" ")[3]);
				logs.add(mes);
			}
			return logs;
		}catch(Exception e){
			return null;
		}
	}
}