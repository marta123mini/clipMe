package com.cs.clipMe.spring.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cs.clipMe.bll.IUserLogic;
import com.cs.clipMe.spring.models.UserFM;

@Controller
public class AdminController {
	public IUserLogic userLogic;
	
	@RequestMapping("/users")
	public ModelAndView getUsers(){
		List<UserFM> userList = UserFM.maps(this.userLogic.getUsers());
		Map<String, Object> model= new HashMap<String, Object>();
		model.put("users", userList);
		return new ModelAndView("admin/users", "model",model);
	}
	@RequestMapping(value="/viewUsers{id}",method=RequestMethod.GET)
	public ModelAndView viewUsers(@ModelAttribute("SpringWeb")UserFM person, ModelMap model){
		model.addAttribute("username",person.getUsername());
		model.addAttribute("password", person.getPassword());
		model.addAttribute("id", person.getId());
		return new ModelAndView("admin/userDetails","model",model);
	}
}