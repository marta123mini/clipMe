package com.cs.clipMe.spring.controllers;

import java.beans.ConstructorProperties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cs.clipMe.bll.IUserLogic;
import com.cs.clipMe.bll.validators.UserValidators;
import com.cs.clipMe.spring.models.UserFM;

@Controller 
public class LoginController {
	private IUserLogic userLogic;
	
	@Autowired
	@Qualifier("userValidator")
	private UserValidators validator;
		
	@ConstructorProperties({"loginControl"})
	@Autowired
	public LoginController (IUserLogic userLog){
		this.userLogic=userLog;
	}
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login(Model modelo){
		modelo.addAttribute("user", new UserFM());
		return "login";
	}
	@RequestMapping(value="/login/signedIn",method=RequestMethod.POST)
	public String LogIn(Model modelo,@ModelAttribute("user") UserFM loggedIn,
			BindingResult result,HttpServletRequest request){
		validator.validate(loggedIn, result);
		if(result.hasErrors()){
			return "/login";
		}else{
		loggedIn=UserFM.map(userLogic.getUser(loggedIn.getUsername(),loggedIn.getPassword()));
		request.getSession().setAttribute("role",userLogic.getRole(UserFM.map(loggedIn)));
		modelo.addAttribute("user",loggedIn);
		return "redirect:/user/home";
		}
	}
	 @RequestMapping("/logout")
     public String logout(HttpSession session ) {
        session.invalidate();
        return "redirect:/user/home";
     }

}