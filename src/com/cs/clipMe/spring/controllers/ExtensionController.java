package com.cs.clipMe.spring.controllers;

import java.beans.ConstructorProperties;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cs.clipMe.bll.IExtensionLogic;
import com.cs.clipMe.spring.models.ExtensionFM;

@Controller
public class ExtensionController {
	public IExtensionLogic extensionLogic;
	
	@ConstructorProperties({"extensionControl"})
	@Autowired
	public ExtensionController(IExtensionLogic extensionLog){
		this.extensionLogic=extensionLog;
	}
	@RequestMapping("/extensions")
	public ModelAndView getExtensions(HttpServletRequest request){
		request.getSession().setAttribute("testVariable", "Answer!");
		List<ExtensionFM> extensionList =ExtensionFM.maps(this.extensionLogic.getExtensions());
		Map<String, Object> model= new HashMap<String, Object>();
		model.put("extensions", extensionList);
		return new ModelAndView("extension/extensions", "model",model);
	}
	@RequestMapping(value="/createExtension",method=RequestMethod.GET)
	public ModelAndView createExtension(){
		return new ModelAndView("extension/addExtension","command", new ExtensionFM());
	}
	@RequestMapping(value="/addExtension",method=RequestMethod.POST)
	public String addExtension(@ModelAttribute("SpringWeb")ExtensionFM extension,
			ModelMap model){
		model.addAttribute("length",extension.getLength());
		model.addAttribute("color", extension.getColor());
		model.addAttribute("texture", extension.getTexture());
		model.addAttribute("price", extension.getPrice());
		model.addAttribute("quantity", extension.getQuantity());
		model.addAttribute("id", extension.getId());
		this.extensionLogic.createExtension(ExtensionFM.map(extension));
		return "redirect:extensions";
	}
	@RequestMapping(value="/extensions/editExtension/{id}",method=RequestMethod.GET)
	public String editExtension(@PathVariable Integer id, Model model){
		model.addAttribute("extension", ExtensionFM.map(this.extensionLogic.getExtensionById(id)));
	      return "extension/editExtension";
	}
	@RequestMapping(value="/extensions/submitEdit/{id}", method=RequestMethod.POST)
	public String editExtension(@ModelAttribute("extension") ExtensionFM ext,
		      @PathVariable Integer id, Model model){
		// We manually assign the id because we disabled it in the JSP page
	     // When a field is disabled it will not be included in the ModelAttribute
		ext.setId(id);
		this.extensionLogic.updateExtension(ExtensionFM.map(ext));
		return "extension/extensionDetails";
	}
	@RequestMapping(value="/extensions/deleteExtension/{id}",method=RequestMethod.GET)
	public String deleteExtension(@ModelAttribute("SpringWeb")ExtensionFM extension,
			@PathVariable Integer id,ModelMap model){
		this.extensionLogic.deleteExtensionById(id);
		return "/extensions";
	}
}