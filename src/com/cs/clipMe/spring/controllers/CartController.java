package com.cs.clipMe.spring.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Form;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.cs.clipMe.spring.models.ExtensionFM;

@Controller
@SessionAttributes({"cart"})
public class CartController {
	
	 @ModelAttribute("cart")
	   public Form populateCart() {
	       return new Form(); // populates form for the first time if its null
	   }

  @RequestMapping(method=RequestMethod.GET)
  public String get(Model model) {
    if(!model.containsAttribute("cart")) {
      model.addAttribute("cart", new ArrayList<ExtensionFM>());
    }
    return "extensions";
  }
  
  @RequestMapping(value="/addCart",method=RequestMethod.POST)
  public String addCart(@ModelAttribute ExtensionFM extension,
      @ModelAttribute("cart") List<ExtensionFM> cart) {
    cart.add(extension);
    return "redirect:/";
  }

}