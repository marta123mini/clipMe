package com.cs.clipMe.spring.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	
	@RequestMapping("/welcome")
	public ModelAndView index(){
		String msg="Welcome to MovieLibrary";
		return new ModelAndView("welcome","message",msg);
	}
}