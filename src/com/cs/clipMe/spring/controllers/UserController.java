package com.cs.clipMe.spring.controllers;

import java.beans.ConstructorProperties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cs.clipMe.bll.*;
import com.cs.clipMe.spring.models.UserFM;

@Controller
public class UserController{
	public IUserLogic userLogic;// = new UserLogic(new UserDAO(new LoggerIO()), new LoggerIO());
	
	@ConstructorProperties({"userControl"})
	@Autowired
	public UserController (IUserLogic userLog){
		this.userLogic=userLog;
	}
	@RequestMapping(value="/user/home",method=RequestMethod.GET)
	public String userHome(HttpServletRequest request){
		request.getSession().setAttribute("role", "Admin");
		return "user/home";
	}
	@RequestMapping(value="/createUser",method=RequestMethod.GET)
	public ModelAndView createUser(){
		return new ModelAndView("user/addUser","command", new UserFM());
	}
	@RequestMapping(value="/addUser",method=RequestMethod.POST)
	public String addUser(@ModelAttribute("SpringWeb")UserFM person,@RequestParam(value="role", required=false) Integer id){
		if(id == null){
			id=1;
		}
		person=UserFM.map(this.userLogic.setRoles(id, UserFM.map(person)));
		this.userLogic.createUser(UserFM.map(person));
		return "redirect:/user/home";
	}
	@RequestMapping(value="/users/editUser/{id}",method=RequestMethod.GET)
	public String editUser(@PathVariable Integer id, Model model){
		model.addAttribute("user", UserFM.map(this.userLogic.getUserById(id)));
	      return "user/editUser";
	}
	@RequestMapping(value="/users/submitEdit/{id}",method=RequestMethod.POST)
	public String editUser(@ModelAttribute("user") UserFM usr,
			@PathVariable Integer id, Model model){
		usr.setId(id);
		this.userLogic.updateUser(UserFM.map(usr));
		return "user/userDetails";
	}
	@RequestMapping(value="/users/deleteUser/{id}",method=RequestMethod.GET)
	public String deleteUser(@ModelAttribute("SpringWeb")UserFM person, 
			@PathVariable Integer id, ModelMap model){
		this.userLogic.deleteUserById(id);
		return "redirect:/";
	}
}