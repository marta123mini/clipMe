package com.cs.clipMe.spring.models;

import java.util.ArrayList;
import java.util.List;

import com.cs.clipMe.bll.UserSM;

public class UserFM {
	
	public String username;
	public String password;
	public String street1;
	public String street2;
	public String city;
	public String state;
	public int zipcode;
	public String firstname;
	public String lastname;
	public long ccn;
	public boolean customer;
	public boolean employee;
	public boolean admin;
	public int id;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStreet1() {
		return street1;
	}
	public void setStreet1(String street1) {
		this.street1 = street1;
	}
	public String getStreet2() {
		return street2;
	}
	public void setStreet2(String street2) {
		this.street2 = street2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public long getCcn() {
		return ccn;
	}
	public void setCcn(long ccn) {
		this.ccn = ccn;
	}
	public boolean isCustomer() {
		return customer;
	}
	public void setCustomer(boolean customer) {
		this.customer = customer;
	}
	public boolean isEmployee() {
		return employee;
	}
	public void setEmployee(boolean employee) {
		this.employee = employee;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public static UserSM map(UserFM newUser){
		UserSM user = new UserSM();
		user.setId(newUser.id);
		user.setUsername(newUser.username);
		user.setPassword(newUser.password);
		user.setStreet1(newUser.street1);
		user.setStreet2(newUser.street2);
		user.setCity(newUser.city);
		user.setState(newUser.state);
		user.setZipcode(newUser.zipcode);
		user.setFirstname(newUser.firstname);
		user.setLastname(newUser.lastname);
		user.setCcn(newUser.ccn);
		user.setCustomer(newUser.customer);
		user.setEmployee(newUser.employee);
		user.setAdmin(newUser.admin);
		return user;
	}
	public static UserFM map(UserSM newUser){
		UserFM user = new UserFM();
		user.setId(newUser.getId());
		user.setUsername(newUser.getUsername());
		user.setPassword(newUser.getPassword());
		user.setStreet1(newUser.getStreet1());
		user.setStreet2(newUser.getStreet2());
		user.setCity(newUser.getCity());
		user.setState(newUser.getState());
		user.setZipcode(newUser.getZipcode());
		user.setFirstname(newUser.getFirstname());
		user.setLastname(newUser.getLastname());
		user.setCcn(newUser.getCcn());
		user.setCustomer(newUser.isCustomer());
		user.setEmployee(newUser.isEmployee());
		user.setAdmin(newUser.isAdmin());
		return user;
	}
	public static List<UserSM> map(List<UserFM> User){
		List<UserSM> newUser = new ArrayList<UserSM>();
		for (UserFM us : User){
		   newUser.add(map(us));
		}
		  return newUser;
	}
	public static List<UserFM> maps(List<UserSM> User){
		List<UserFM> newUser = new ArrayList<UserFM>();
		for (UserSM us : User){
		   newUser.add(map(us));
		}
		  return newUser;
	}
}