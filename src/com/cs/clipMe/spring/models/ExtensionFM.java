package com.cs.clipMe.spring.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.cs.clipMe.bll.ExtensionSM;

public class ExtensionFM implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String length;
	public String color;
	public String texture;
	public int price;
	public int quantity;
	public int id;
	
	public String getLength() {
		return length;
	}
	public void setLength(String length) {
		this.length = length;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getTexture() {
		return texture;
	}
	public void setTexture(String texture) {
		this.texture = texture;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public static ExtensionSM map(ExtensionFM extensions){
		  ExtensionSM newExtension=new ExtensionSM();
		  newExtension.setId(extensions.id);
		  newExtension.setLength(extensions.length);
		  newExtension.setColor(extensions.color);
		  newExtension.setTexture(extensions.texture);
		  newExtension.setPrice(extensions.price);
		  newExtension.setQuantity(extensions.quantity);
		  return newExtension;
	}
	public static  ExtensionFM map(ExtensionSM extensions){
		  ExtensionFM newExtension=new ExtensionFM();
		  newExtension.setId(extensions.getId());
		  newExtension.setLength(extensions.getLength());
		  newExtension.setColor(extensions.getColor());
		  newExtension.setTexture(extensions.getTexture());
		  newExtension.setPrice(extensions.getPrice());
		  newExtension.setQuantity(extensions.getQuantity());
		  return newExtension;
	}
	public static List<ExtensionSM> map(List<ExtensionFM> extensions){
		  List<ExtensionSM> newExtension=new ArrayList<ExtensionSM>();
		  for(ExtensionFM extension:extensions){
			 newExtension.add(map(extension));
		  }
		  return newExtension;
	}
	public static List<ExtensionFM> maps(List<ExtensionSM> Extension){
		  List<ExtensionFM> newExtension = new ArrayList<ExtensionFM>();
		  for (ExtensionSM us : Extension){
		   newExtension.add(map(us));
		  }
		  return newExtension;
	}
}