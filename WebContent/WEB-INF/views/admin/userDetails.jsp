<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>clipMe</title>
</head>
<body>
	<h2>User Information</h2>
	<table>
		<tr>
			<td>Username</td>
			<td>${username}</td>
		</tr>
		<tr>
			<td>Password</td>
			<td>${password}</td>
		</tr>
		<tr>
			<td>Street1</td>
			<td>${street1}</td>
		</tr>
		<tr>
			<td>Street2</td>
			<td>${street2}</td>
		</tr>
		<tr>
			<td>City</td>
			<td>${city}</td>
		</tr>
		<tr>
			<td>State</td>
			<td>${state}</td>
		</tr>
		<tr>
			<td>Zipcode</td>
			<td>${zipcode}</td>
		</tr>
		<tr>
			<td>Firstname</td>
			<td>${firstname}</td>
		</tr>
		<tr>
			<td>Lastname</td>
			<td>${lastname}</td>
		</tr>
		<tr>
			<td>Ccn</td>
			<td>${ccn}</td>
		</tr>
		<tr>
				<c: forEach test="${customer}" var="customer">
			<td>Customer</td>
			<td>${customer}</td>
				<c:choose>
					<c:when>
						<input type ="checkbox" name="customer" value="${id}" checked="checked">
						${customer}&nbsp;
					</c:when>
					<c:otherwise>
						<input type="checkbox" name="customer" value="${id}">${customer}>					
					</c:otherwise>
				</c:choose>
				</c: forEach>
		</tr>
		<tr>
			<td>Employee</td>
			<td>${employee}</td>
		</tr>
		<tr>
			<td>Admin</td>
			<td>${admin}</td>
		</tr>
	</table>
</body>
</html>