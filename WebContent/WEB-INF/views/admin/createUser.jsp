<%@ include file="/WEB-INF/includes.jsp"%>
<%@ include file="/WEB-INF/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Information</title>
</head>
<body>
	<h2>User Information</h2>
	<form:form method="POST" action="/clipMe/addUser">
		<table>
			<tr>
				<td><form:label path="username">Username: </form:label></td>
				<td><form:input path="username" /></td>
			</tr>
			<tr>
				<td><form:label path="password">Password: </form:label></td>
				<td><form:password path="password" showPassword="false" /></td>
			</tr>
			<tr>
				<td><form:label path="street1">Street1: </form:label></td>
				<td><form:input path="street1" /></td>
			</tr>
			<tr>
				<td><form:label path="street2">Street2: </form:label></td>
				<td><form:input path="street2" /></td>
			</tr>
			<tr>
				<td><form:label path="city">City: </form:label></td>
				<td><form:input path="city" /></td>
			</tr>
			<tr>
				<td><form:label path="state">State: </form:label></td>
				<td><form:input path="state" /></td>
			</tr>
			<tr>
				<td><form:label path="zipcode">Zipcode: </form:label></td>
				<td><form:input path="zipcode" /></td>
			</tr>
			<tr>
				<td><form:label path="firstname">Firstname: </form:label></td>
				<td><form:input path="firstname" /></td>
			</tr>
			<tr>
				<td><form:label path="lastname">Lastname: </form:label></td>
				<td><form:input path="lastname" /></td>
			</tr>
			<tr>
				<td><form:label path="ccn">Ccn: </form:label></td>
				<td><form:password path="ccn" /></td>
			</tr>
			<c:if test="${not empty role}">		
			<tr>
				<td>Roles:<select path="role" name="role" id="role"
					onchange="">
						<option value="">Choose Role</option>
						<option value="1">Customer</option>
						<option value="2">Employee</option>
						<option value="3">Admin</option>
				</select></td>
			</tr>
			</c:if>
			<tr>
				<td COLSPAN="1"><input type="submit" value="Create" /></td>
			</tr>
		</table>
	</form:form>
</body>

<%@ include file="/WEB-INF/footer.jsp"%>