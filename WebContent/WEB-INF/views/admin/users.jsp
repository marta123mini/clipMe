<%@ include file="/WEB-INF/includes.jsp"%>
<%@ include file="/WEB-INF/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<h2>List of Users</h2>

<table>
	<thead>
		<th>Username</th>
		<th></th>
	</thead>
	<c:forEach var="usr" items="${model.users}">
		<tr>
			<td>${usr.username}</td>
			<td><a
				href="<c:url value="/users/editUser/${usr.id}">
					<c:param name="userId" value="${usr.id}"/></c:url>">Edit</a>
				<a
				href="<c:url value="/users/deleteUser/${usr.id}">
					<c:param name="userId" value="${usr.id}"/></c:url>">Delete</a>
				<a
				href="<c:url value="/admin/viewUsers/${usr.id}">
					<c:param name="userId" value="${usr.id}"/></c:url>">Details</a>
			</td>
		</tr>
	</c:forEach>
</table>
<p align="left">
	<a href="createUser">Create User</a>
</p>


<%@ include file="/WEB-INF/footer.jsp"%>