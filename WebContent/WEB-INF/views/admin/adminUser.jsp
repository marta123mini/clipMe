<%@ include file="/WEB-INF/includes.jsp"%>
<%@ include file="/WEB-INF/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<h2>List of Users</h2>

<table>
	<thead>
		<th>Username</th>
		<th>Password</th>
		<th>Street1</th>
		<th>Street2</th>
		<th>City</th>
		<th>State</th>
		<th>Zipcode</th>
		<th>Firstname</th>
		<th>Lastname</th>
		<th>Ccn</th>
	</thead>
	<c:forEach var="usr" items="${model.users}">
		<tr>
			<td>${usr.username}</td>
			<td><a
				href="<c:url value="/users/editUser/${usr.id}">
					<c:param name="userId" value="${usr.id}"/></c:url>">Edit</a>
				<a
				href="<c:url value="/users/deleteUser/${usr.id}">
					<c:param name="userId" value="${usr.id}"/></c:url>">Delete</a>
			</td>
		</tr>
	</c:forEach>
</table>


<%@ include file="/WEB-INF/footer.jsp"%>