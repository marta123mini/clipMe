<%@ include file="/WEB-INF/includes.jsp"%>
<%@ include file="/WEB-INF/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true" language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>clipMe-Login</title>
</head>
<body>
	<h2>       <- Log In to clipYou!      </h2>
	
	<h3>Welcome, Please Login!</h3>
	<form:form method="POST" modelAttribute="user" action="/clipMe/login/signedIn">
		<table>
			<tr>
				<td><form:label path="username">Username: </form:label></td>
				<td><form:input path="username" /></td>
			</tr>
			<tr>
				<td><form:label path="password">Password: </form:label></td>
				<td><form:password path="password" showPassword="false" /></td>
			</tr>
			<tr>
				<td COLSPAN="1"><input type="submit" value="LogIn" /></td>
				<!-- Need to add a forgot password link, 
				add link for forgotpassword.jsp which will have a 
				password field and a reconfirm password field -->
			</tr>
		</table>
	</form:form>
</body>

<%@ include file="/WEB-INF/footer.jsp"%>