<%@ include file="/WEB-INF/includes.jsp"%>
<%@ include file="/WEB-INF/header.jsp"%>


<h2>Edit User Profile</h2>

<body>
	<form:form commandName="user" method="POST" action="/clipMe/users/submitEdit/${user.id}">
		<table>
			<tr>
				<td><form:label path="username">Username: </form:label></td>
				<td><form:input path="username" /></td>
			</tr>
			<tr>
				<td><form:label path="password">Password: </form:label></td>
				<td><form:input path="password"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="street1">Street1: </form:label></td>
				<td><form:input path="street1" /></td>
			</tr>
			<tr>
				<td><form:label path="street2">Street2: </form:label></td>
				<td><form:input path="street2" /></td>
			</tr>
			<tr>
				<td><form:label path="city">City: </form:label></td>
				<td><form:input path="city" /></td>
			</tr>
			<tr>
				<td><form:label path="state">State: </form:label></td>
				<td><form:input path="state" /></td>
			</tr>
			<tr>
				<td><form:label path="zipcode">Zipcode: </form:label></td>
				<td><form:input path="zipcode" /></td>
			</tr>
			<tr>
				<td><form:label path="firstname">Firstname: </form:label></td>
				<td><form:input path="firstname" /></td>
			</tr>
			<tr>
				<td><form:label path="lastname">Lastname: </form:label></td>
				<td><form:input path="lastname" /></td>
			</tr>
			<tr>
				<td><form:label path="ccn">Ccn: </form:label></td>
				<td><form:password path="ccn" /></td>
			</tr>
			<tr>
				<td colspan="1"><input type="submit" value="Edit" /></td>
			</tr>
		</table>
	</form:form>

</body>


<%@ include file="/WEB-INF/footer.jsp"%>
