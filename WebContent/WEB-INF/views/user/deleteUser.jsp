<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>clipMe</title>
</head>
<body>
	<h2>Are you sure you want to delete the user below?</h2>
	<form:form method="GET" action="/clipMe/users${user.id}">
		<table>
			<tr>
				<td>Username</td>
				<td>${username}</td>
			</tr>
			<tr>
				<td>Password</td>
				<td>${password}</td>
			</tr>
			<tr>
				<td>Street1</td>
				<td>${street1}</td>
			</tr>
			<tr>
				<td>Street2</td>
				<td>${street2}</td>
			</tr>
			<tr>
				<td>City</td>
				<td>${city}</td>
			</tr>
			<tr>
				<td>State</td>
				<td>${state}</td>
			</tr>
			<tr>
				<td>Zipcode</td>
				<td>${zipcode}</td>
			</tr>
			<tr>
				<td>Firstname</td>
				<td>${firstname}</td>
			</tr>
			<tr>
				<td>Lastname</td>
				<td>${lastname}</td>
			</tr>
			<tr>
				<td>Ccn</td>
				<td>${ccn}</td>
			</tr>
			<tr>
				<td COLSPAN="2"><input type="submit" value="Delete" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>