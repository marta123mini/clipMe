<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>USER INFO</title>
</head>
<body>
	<h2>Enter your information below</h2>
	<form:form method="POST" action="/clipMe/addUser">
		<table>
			<tr>
				<td><form:label path="username">Username: </form:label></td>
				<td><form:input path="username" /></td>
			</tr>
			<tr>
				<td><form:label path="password">Password: </form:label></td>
				<td><form:password path="password" showPassword="false" /></td>
			</tr>
			<tr>
				<td><form:label path="street1">Street1: </form:label></td>
				<td><form:input path="street1" /></td>
			</tr>
			<tr>
				<td><form:label path="street2">Street2: </form:label></td>
				<td><form:input path="street2" /></td>
			</tr>
			<tr>
				<td><form:label path="city">City: </form:label></td>
				<td><form:input path="city" /></td>
			</tr>
			<tr>
				<td><form:label path="state">State: </form:label></td>
				<td><form:input path="state" /></td>
			</tr>
			<tr>
				<td><form:label path="zipcode">Zipcode: </form:label></td>
				<td><form:input path="zipcode" /></td>
			</tr>
			<tr>
				<td><form:label path="firstname">Firstname: </form:label></td>
				<td><form:input path="firstname" /></td>
			</tr>
			<tr>
				<td><form:label path="lastname">Lastname: </form:label></td>
				<td><form:input path="lastname" /></td>
			</tr>
			<tr>
				<td><form:label path="ccn">Ccn: </form:label></td>
				<td><form:password path="ccn" /></td>
			</tr>
			<tr>
				<td COLSPAN="1"><input type="submit" value="Create" /></td>
			</tr>
		</table>
	</form:form>
</body>