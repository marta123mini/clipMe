<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ include file="/WEB-INF/includes.jsp"%>
<%@ include file="/WEB-INF/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<h2>List of Extensions</h2>

<table>
	<thead>
		<th>Length</th>
		<th>Color</th>
		<th>Texture</th>
		<th>Price</th>
		<th>Quantity</th>
	</thead>
	<form method="POST" commandName="exts">
		<table>
			<tr>
				<td><select name="dd1" onchange="submit()">
						<option value="">Select Extension</option>
						<c:forEach items="${model.extensions}" var="option">
							<option value="color">${option.length}</option>
							<option value="color">${option.color}</option>
							<option value="color">${option.texture}</option>
							<option value="color">${option.price}</option>
						</c:forEach>
				</select></td>
				<td><input type="submit" value="Generate Extension"></td>
			</tr>
		</table>
	</form>
	<c:forEach var="hair" items="${model.extensions}">
		<tr>
			<td>${hair.length}</td>
			<td>${hair.color}</td>
			<td>${hair.texture}</td>
			<td>${hair.price}</td>
			<td>${hair.quantity}</td>
			<td>
				<a
				href="<c:url value="/extensions/editExtension/${hair.id}">
					<c:param name="extensionId" value="${hair.id}"/></c:url>">Edit</a>
				<a
				href="<c:url value="/extensions/deleteExtension/${hair.id}">
					<c:param name="extensionId" value="${hair.id}"/></c:url>">Delete</a>
			</td>
		</tr>
	</c:forEach>
</table>

<p align="left">
	<a href="createExtension">Create Extension</a>
</p>


<%@ include file="/WEB-INF/footer.jsp"%>
