<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>clipMe</title>
</head>
<body>
	<h2>Are you sure you want to delete the extension below?</h2>
	<form:form method="GET" action="/clipMe/extensions/${extension.id}">
		<table>
			<tr>
				<td>Length</td>
				<td>${length}</td>
			</tr>
			<tr>
				<td>Color</td>
				<td>${color}</td>
			</tr>
			<tr>
				<td>Texture</td>
				<td>${texture}</td>
			</tr>
			<tr>
				<td>Price</td>
				<td>${price}</td>
			</tr>
			<tr>
				<td>Quantity</td>
				<td>${quantity}</td>
			</tr>
			<tr>
				<td COLSPAN="2"><input type="submit" value="Delete" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>