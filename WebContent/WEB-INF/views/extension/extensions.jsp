<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ include file="/WEB-INF/includes.jsp"%>
<%@ include file="/WEB-INF/userHeader.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<h2>clipYou!</h2>

<table>
	<thead>
		<th>Length</th>
		<th>Color</th>
		<th>Texture</th>
		<th>Price</th>
		<th>Quantity</th>
	</thead>
	
	<c:forEach var="hair" items="${model.extensions}">
		<tr>
			<td>${hair.length}</td>
			<td>${hair.color}</td>
			<td>${hair.texture}</td>
			<td>${hair.price}</td>
			<td>${hair.quantity}</td>
		</tr>
	</c:forEach>
</table>
	<p>Add Extension to Cart</p>
  <form:form method="POST" action="/clipMe/addCart">
    	Length: <input type="text" name="length" /> Color: <input type="text" name="color" /> Texture: <input type="text" name="texture" />  Price: <input type="text" name="price" />
    <button>Add Extension</button>
  </form:form>

<%@ include file="/WEB-INF/userFooter.jsp"%>
