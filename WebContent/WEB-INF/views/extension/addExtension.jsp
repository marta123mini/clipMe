<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/WEB-INF/includes.jsp"%>
<%@ include file="/WEB-INF/header.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>clipMe - Create New Extension</title>
</head>
<body>
	<h2>Create a New Extension</h2>
	<form:form method="POST" action="/clipMe/addExtension">
		<table>
			<tr>
				<td><form:label path="length">Length: </form:label></td>
				<td><form:input path="length" /></td>
			</tr>
			<tr>
				<td><form:label path="color">Color: </form:label></td>
				<td><form:input path="color"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="texture">Texture: </form:label></td>
				<td><form:input path="texture" /></td>
			</tr>
			<tr>
				<td><form:label path="price">Price: </form:label></td>
				<td><form:input path="price" /></td>
			</tr>
			<tr>
				<td><form:label path="quantity">Quantity: </form:label></td>
				<td><form:input path="quantity" /></td>
			</tr>
			<tr>
				<td colspan="1"><input type="submit" value="Create" /></td>
			</tr>
		</table>
	</form:form>
</body>

<%@ include file="/WEB-INF/footer.jsp"%>