<%@ include file="/WEB-INF/includes.jsp"%>
<%@ include file="/WEB-INF/header.jsp"%>


<h2>Create a New Extension</h2>

<body>
	<form:form commandName="extension" method="POST" action="/clipMe/extensions/submitEdit/${extension.id}">
		<table>
			<tr>
				<td><form:label path="length">Length: </form:label></td>
				<td><form:input path="length"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="color">Color: </form:label></td>
				<td><form:input path="color"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="texture">Texture: </form:label></td>
				<td><form:input path="texture"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="price">Price: </form:label></td>
				<td><form:input path="price"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="quantity">Quantity: </form:label></td>
				<td><form:input path="quantity"></form:input></td>
			</tr>
			<tr>
				<td colspan="1"><input type="submit" value="Edit" /></td>
			</tr>
		</table>
	</form:form>

</body>


<%@ include file="/WEB-INF/footer.jsp"%>
