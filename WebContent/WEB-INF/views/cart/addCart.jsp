<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ include file="/WEB-INF/includes.jsp"%>
<%@ include file="/WEB-INF/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Shopping Cart</title>
</head>
<body>
  <h1>Your Shopping Cart</h1>
  <c:forEach items="${cart}" var="extension">
    Length: ${extension.length}, 
    Color: ${extension.color},
    Texture: ${extension.texture},
    Price: ${extension.price}
    <br />
  </c:forEach>
</body>
</html>